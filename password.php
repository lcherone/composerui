<?php
if(!defined('RUN')) exit(header('Location: index.php'));

//um... changeme
$password = "changeme";

if (!isset($_SERVER['PHP_AUTH_USER']) || $_SERVER['PHP_AUTH_PW'] !== $password) {
    header('WWW-Authenticate: Basic realm="ComposerUI"');
    header('HTTP/1.0 401 Unauthorized');
    exit;
}
