<?php
if(!defined('RUN')) exit(header('Location: index.php'));

define('CWD', dirname(__FILE__));
define('VENDOR_DIR', 'vendor'); //should match one in composer or scanner will pick up vendor projects

function projects() {
    $projects = array();
    foreach(new RecursiveIteratorIterator(
                new RecursiveDirectoryIterator(CWD.'/../', FilesystemIterator::SKIP_DOTS),
                RecursiveIteratorIterator::CHILD_FIRST) as $i=>$path) {

        if(file_exists($path->getPathname().'/composer.json') &&
            stristr($path->getPathname(), VENDOR_DIR) === FALSE) {
            $projects[] = realpath($path->getPathname());
        }
    }
    return $projects;
}
