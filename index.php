<?php
define('RUN', true);

include 'password.php';
include 'functions.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <title>ComposerUI</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css" rel="stylesheet">
        <link href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/css/bootstrap-select.min.css" rel="stylesheet">
        <link href="//maxcdn.bootstrapcdn.com/bootswatch/3.3.1/readable/bootstrap.min.css" rel="stylesheet">
        <script type="text/javascript" src="//code.jquery.com/jquery-2.1.1.min.js"></script>
        <script type="text/javascript" src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.3/js/bootstrap-select.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                check();
                $('.selectpicker').selectpicker();

                $('#myModal').on('shown.bs.modal', function (event) {

                    var project_id = $(event.relatedTarget).data('project-id');

                    $('#myModalLabel').html(project_id);
                    $('.modal-body').html('This is the proects composer.json for ' + project_id);
                });
            });

            function url()
            {
                return 'main.php';
            }

            function call(func)
            {
                $("#console").html('');
                $("#console").append("Executing "+func+", please wait\n");
                $.post('main.php', {
                    "path": $("#path").val(),
                    "command": func,
                    "function": "command"
                }, function(data){
                    $("#console").append(data);
                    $("#console").append("\nExecution Ended");
                    console_scroll();
                });
            }

            function check()
            {
                $("#console").append('\nloading...\n');
                $.post(url(), {
                    "function": "getStatus",
                    "password": $("#password").val()
                }, function(data) {
                    if (data.composer_extracted) {
                        $("#console").html("Ready. All commands are available.\n");
                        $("button").removeClass('disabled');
                    } else if(data.composer) {
                        $.post(url(), {
                            "password": $("#password").val(),
                            "function": "extractComposer",
                        }, function(data) {
                            $("#console").append(data);
                            window.location.reload();
                        }, 'text');
                    } else {
                        $("#console").html("Please wait till composer is being installed...\n");
                        $.post(url(), {
                            "password": $("#password").val(),
                            "function": "downloadComposer",
                        }, function(data) {
                            $("#console").append(data);
                            check();
                        }, 'text');
                    }
                });
            }

            function console_scroll() {
                 $('#console').animate({"scrollTop": $('#console')[0].scrollHeight}, "slow");
            }
        </script>
        <style>
            #console {
                width:100%;
                height:350px;
                overflow-y:scroll;
                overflow-x:hidden;
            }
        </style>
    </head>
    <body>
        <div class="row-fluid">
            <div class="col-lg-1"></div>
            <div class="col-lg-10">
                <h1>ComposerUI</h1>
                <hr/>
                <div class="form-inline">
                    <h3>Projects</h3>
                    <div class="form-group">
                        <select id="path" class="selectpicker" data-width="100%" class="form-control" onChange="$('#view-composer').data('project-id', this.value)">
                        <?php foreach (projects() as $key=>$project) : $initial = ($key == 0) ? $project : $initial  ?>
                            <option><?php echo $project; ?></option>
                        <?php endforeach; ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <button id="view-composer" data-toggle="modal" data-target="#myModal" data-project-id="<?php echo $initial; ?>" class="btn btn-success disabled">View projects composer.json</button>
                    </div>
                </div>

                <h3>Commands</h3>
                <div class="form-inline">
                    <button id="install" onclick="call('install')" class="btn btn-success disabled">install</button>
                    <button id="update" onclick="call('update')" class="btn btn-success disabled">update</button>
                </div>
                <h3>Console Output</h3>
                <pre id="console" class="well"></pre>
                <button id="clear-console" onclick="$('#console').html('');" class="btn btn-success pull-right">Clear console</button>
            </div>
            <div class="col-lg-1"></div>
        </div>
        <!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
              </div>
              <div class="modal-body">
                Work in progress!
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
              </div>
            </div>
          </div>
        </div>
    </body>
</html>
